﻿#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <math.h>

struct Adjacency {
    bool edge = false;
    bool visited = false; 

    Adjacency() {
        edge = false;
        visited = false;
    }
};

struct Edge {
    int A, B, distance;

    Edge() {
        A = 0;
        B = 0;
        distance = 0;
    }

    Edge(int A, int B, int distance) {
        this->A = A;
        this->B = B;
        this->distance = distance;
    }

    static int comparator(Edge p, Edge q) {
        return (p.distance < q.distance);
    }
};

struct Tree {
    std::vector<Edge> Edges;
    std::vector<int> Path;
    int nv;
    Adjacency** adjacencyMatrix;

    Tree() {
        nv = 0;
        adjacencyMatrix = new Adjacency * [nv + 1];

        for (int i = 0; i <= nv; i++) {
            adjacencyMatrix[i] = new Adjacency[nv + 1];
        }
    }

    Tree(int index, int nv) {
        Edges.push_back(Edge(index, 0, 0));
        this->nv = nv;
        adjacencyMatrix = new Adjacency* [nv+1];

        for (int i = 0; i <= nv; i++) {
            adjacencyMatrix[i] = new Adjacency[nv+1];
        }
    }

    Tree(const Tree& T) {
        Edges = T.Edges;
        this->nv = T.nv;
        adjacencyMatrix = new Adjacency * [nv + 1];

        for (int i = 0; i <= nv; i++) {
            adjacencyMatrix[i] = new Adjacency[nv + 1];
        }
    }

    Tree& operator=(const Tree& T) {
        Edges = T.Edges;
        this->nv = T.nv;
        adjacencyMatrix = new Adjacency * [nv + 1];

        for (int i = 0; i <= nv; i++) {
            adjacencyMatrix[i] = new Adjacency[nv + 1];
        }

        return *this;
    }

    void addEdge(Edge e) {
        Edges.push_back(e);
    }


    int findIfEdgeExists(int A, int B) {
        bool verticeA = false;
        bool verticeB = false;

        for (int i = 0; i < Edges.size(); i++) {
            if ( (Edges[i].A == A) || (Edges[i].B == A) ) verticeA = true;
            if ( (Edges[i].A == B) || (Edges[i].B == B) ) verticeB = true;
        }

        /*
        std::cout << verticeA << " " << verticeB << "\n";
        if (((verticeA && !verticeB) || (!verticeA && verticeB))) {
            std::cout << "RESULT: " << 1 << "\n";
        }
        else {
            std::cout << "RESULT: " << 0 << "\n";
        }
        */

        if (verticeA && !verticeB) {
            return B;
        } else if (!verticeA && verticeB) {
            return A;
        } else {
            return -1;
        }
        //return ((verticeA && !verticeB) || (!verticeA && verticeB)) ? true : false;
    }

    int findDistance(int A, int B) {
        for (int i = 0; i < Edges.size(); i++) {
            if ((Edges[i].A == A) && (Edges[i].B == B)) return Edges[i].distance;
            if ((Edges[i].A == B) && (Edges[i].B == A)) return Edges[i].distance;
        }

        return 0;
    }


    bool findToDelete(int A) {
        bool verticeA = false;

        for (int i = 0; i < Edges.size(); i++) {
            if ((Edges[i].A == A) || (Edges[i].B == A)) verticeA = true;
        }
    }

    void printEdges() {
        for (int i = 0; i < Edges.size(); i++) {
            std::cout << "Edge: " << Edges[i].A << " -> " << Edges[i].B << ": " << Edges[i].distance << "\n";
        }
    }

    void collapse() {
        sort(Edges.begin(), Edges.end(), Edge::comparator);

        while (Edges.front().distance == 0) {
            Edges.erase(Edges.begin());
        }
    }

    void createAdjacencyMatrix() {
        for (int i = 0; i < Edges.size(); i++) {
            adjacencyMatrix[Edges[i].A][Edges[i].B].edge = true;
            adjacencyMatrix[Edges[i].B][Edges[i].A].edge = true;
        }
    }

    void showAdjacencyMatrix() {
        for (int i = 1; i <= nv; i++) {
            for (int j = 1; j <= nv; j++) {
                std::cout << adjacencyMatrix[i][j].edge << " ";
            }
            std::cout << "\n";
        }
    }

    void resetAdjacencyMatrix() {
        for (int i = 1; i <= nv; i++) {
            for (int j = 1; j <= nv; j++) {
                adjacencyMatrix[i][j].visited = false;
            }
        }
    }
    
    void visitNode(int A, int B) {
        Path.push_back(A);

        /*
        std::cout << "\nPATH\n";
        for (int i = 0; i < Path.size(); i++) {
            std::cout << Path[i] << " ";
        }
        std::cout << "\nPATH\n";
        */


        if (A == B) return;

        for (int i = 1; i <= nv; i++) {

            if (adjacencyMatrix[A][i].edge && !adjacencyMatrix[A][i].visited) {
                //std::cout << A << " " << i << "\n";
                adjacencyMatrix[A][i].visited = true;
                adjacencyMatrix[i][A].visited = true;

                visitNode(i, B);
                if (Path.back() == B) return;
                Path.pop_back();
            }
            

        }
    }

    void DFS(int A, int B) {
        resetAdjacencyMatrix(); // resets visits
        visitNode(A, B);

        for (int i = 0; i < Path.size(); i++) {
            std::cout << Path[i] << " ";
        }
    }

    void solve(int A, int B, int ppl) {
        DFS(A, B);

        int minimum = findDistance(Path[0], Path[1]);

        for (int i = 1; i < Path.size() - 1; i++) {
            int distance = findDistance(Path[i], Path[i + 1]);
            if (distance < minimum) minimum = distance;
        }

        double result = (double)ppl/(double)minimum;
        std::cout << "= " << ppl << "/" << minimum << " => " << ceil(result);


        
        Path.clear();

    }

    ~Tree() {
        //std::cout << " destruktor ";

        for (int i = 0; i <= nv; i++) {
            delete[] adjacencyMatrix[i];
        }

        delete[] adjacencyMatrix;
    }

};

struct Graph {
    int nv; // number of vertices
    int ne; // number of edges
    std::vector<Edge> Edges;
    std::vector<Tree> Forest;

    Graph() {
        nv = 0;
        ne = 0;
    }

    Graph(int nv, int ne) {
        this->nv = nv;
        this->ne = ne;
    }

    void addEdge(int index, int A, int B, int distance) {
        Edges.push_back(Edge(A, B, distance));
    }

    void sortEdges() {
        sort(Edges.begin(), Edges.end(), Edge::comparator);
    }

    void printEdges() {
        for (int i = 0; i < Edges.size(); i++) {
            std::cout << "Edge: " << Edges[i].A << " -> " << Edges[i].B << ": " << Edges[i].distance << "\n";
        }
    }

    int findTreeIndex(int x, int e) {
        for (int i = 0; i < Forest.size(); i++) {
            if (i == x) continue;
            for (int j = 0; j < Forest[i].Edges.size(); j++) {
                if ((Forest[i].Edges[j].A == e) || (Forest[i].Edges[j].B == e)) {
                    return i;
                }
            }
        }
        return 0; // upewnij sie ze tego nie zwroci na pewno
    }

    void copyTree(int copyFrom, int copyTo) {
        for (int i = 0; i < Forest[copyFrom].Edges.size(); i++) {
            Forest[copyTo].addEdge(Forest[copyFrom].Edges[i]);
        }
    }

    void findMST() {
        for (int i = 0; i < nv; i++) {
            Forest.push_back(Tree(i+1, nv));
            //std::cout << "whot";
        }

        sortEdges();

        while ( (!Edges.empty()) && (Forest.size() > 1) ) {
            bool skip = true;
            for (int j = 0; j < Forest.size(); j++) {
                int e = Forest[j].findIfEdgeExists(Edges.back().A, Edges.back().B);
                if (e >= 0) {
                    //std::cout << "e: " << e;
                    int index = findTreeIndex(j, e);
                    //Forest[index].printEdges();
                    //std::cout << "index znalezionego: " << index;
                    copyTree(index, j);
                    Forest[j].addEdge(Edges.back());
                    Edges.pop_back();
                    Forest.erase(Forest.begin() + index);
                    
                    //std::cout << "\n";
                    //Forest[j].printEdges();
                    //std::cout << "\n";
                    skip = false;
                    //printEdges();
                    break;
                }
            }
            if (skip) Edges.pop_back(); // po przejsciu i nie znalezieniu dobrej krawedzi to zpopowanie krawedzi
        }

        Forest.front().collapse();

        Forest.front().printEdges();

        Forest.front().createAdjacencyMatrix();
    }

    void find(int A, int B, int ppl) {
        Forest.front().solve(A, B, ppl);
    }
};

int main()
{
    int nv = 0, ne = 0, A = 0, B = 0, distance = 0, ppl = 0;

    std::cin >> nv >> ne;

    Graph G(nv, ne);

    for (int i = 0; i < ne; i++) {
        std::cin >> A >> B >> distance;
        G.addEdge(i, A, B, distance-1);
    }
    
    G.findMST();

    while (true) {
        std::cin >> A >> B;
        if ((A == 0) && (B == 0)) break;
        std::cin >> ppl;
        G.find(A, B, ppl);
        std::cout << "\n";
    }

    
    std::cin >> A;
    return 0;
}