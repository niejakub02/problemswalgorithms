﻿#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>

struct Edge {
    bool who;
    int A, B, fatigue;

    Edge() {
        who = 0;
        A = 0;
        B = 0;
        fatigue = 0;
    }

    Edge(bool who, int A, int B, int fatigue) {
        this->who = who;
        this->A = A;
        this->B = B;
        this->fatigue = fatigue;
    }

    void init(bool who, int A, int B, int fatigue) {
        this->who = who;
        this->A = A;
        this->B = B;
        this->fatigue = fatigue;
    }
};

struct Subgraph {
    std::vector<Edge> Edges;
    int** fatigueArray;
    int** stepsArray;
    int nv;

    Subgraph() {
        nv = 0;
        fatigueArray = nullptr;
        stepsArray = nullptr;
    }

    Subgraph(int index) {
        nv = 0;
        fatigueArray = nullptr;
        stepsArray = nullptr;
    }

    void init(int nv) {
        this->nv = nv;

        fatigueArray = new int* [nv];

        for (int i = 0; i < nv; i++) {
            fatigueArray[i] = new int[nv];
        }

        stepsArray = new int* [nv];

        for (int i = 0; i < nv; i++) {
            stepsArray[i] = new int[nv];
        }
    }

    void addEdge(Edge e) {
        Edges.push_back(e);
    }

    void createFatigueArray() {
        for (int i = 0; i < nv; i++) {
            for (int j = 0; j < nv; j++) {
                if (i == j) {
                    fatigueArray[i][j] = 0;
                    continue;
                }
                else {
                    fatigueArray[i][j] = findFatigue(i + 65, j + 65);
                }
            }
        }
    }

    int findFatigue(int A, int B) {
        int minimum = -1;

        for (size_t j = 0; j < Edges.size(); j++) {
            if ((Edges[j].A == A) && (Edges[j].B == B)) {
                if ((minimum < 0) || (Edges[j].fatigue < minimum)) minimum = Edges[j].fatigue;
            }
        }

        return minimum;
        //return -1;
    }

    void showFatigueArray() {
        for (int i = 0; i < nv; i++) {
            for (int j = 0; j < nv; j++) {
                std::cout << fatigueArray[i][j] << " ";
            }
            std::cout << "\n";
        }
    }
    void createStepsArray(int sX) {
        int fatigue = 0;

        for (int j = 0; j < nv; j++) {
            stepsArray[0][j] = fatigueArray[sX-65][j];
        }

        for (int i = 1; i < nv; i++) {
            for (int j = 0; j < nv; j++) {
                stepsArray[i][j] = stepsArray[i - 1][j];

                
                for (int x = 0; x < nv; x++) {
                    if ( (fatigueArray[x][j] > 0) && (stepsArray[i - 1][x] > 0) ) { // Znak w drugim warunku byl odwrocony
                        fatigue = fatigueArray[x][j] + stepsArray[i - 1][x];

                        if ( (fatigue < stepsArray[i][j]) || (stepsArray[i][j] < 0) ) stepsArray[i][j] = fatigue;
                    }
                }
                
                /* //DZIALA ALE NIE WYKORZYTSUJE FATIGUEARRAY ZA BARDZO
                for (int x = 0; x < Edges.size(); x++) {
                    if (Edges[x].B == (j + 65)) {
                        if (stepsArray[i - 1][Edges[x].A - 65] < 0) continue;

                        fatigue = Edges[x].fatigue + stepsArray[i - 1][Edges[x].A - 65];

                        if ( (fatigue < stepsArray[i][j]) || (stepsArray[i][j] < 0) ) stepsArray[i][j] = fatigue;
                    }
                }
                */
                
            }
        }
    }

    void showStepsArray() {
        for (int i = 0; i < nv; i++) {
            for (int j = 0; j < nv; j++) {
                std::cout << stepsArray[i][j] << " ";
            }
            std::cout << "\n";
        }
    }

    void getMeetupPoints() {
        for (int j = 0; j < nv; j++) {
            if (stepsArray[nv - 1][j] >= 0) std::cout << (char)(j + 65) << " ";
        }
        std::cout << "\n";
    }

    void printEdges() {
        for (size_t i = 0; i < Edges.size(); i++) {
            std::cout << "Edge " << Edges[i].who << ": " << (char)Edges[i].A << " -> " << (char)Edges[i].B << ": " << Edges[i].fatigue << "\n";
        }
    }

    ~Subgraph() {
        for (int i = 0; i < nv; i++) {
            delete[] fatigueArray[i];
        }

        delete[] fatigueArray;
    }
};

struct Graph {
    int ne; // number of edges
    int nv;
    std::vector<Edge> Edges;
    std::vector<Subgraph> Subgraphs;

    Graph() {
        ne = 0;
        nv = 0;
    }

    void init(int ne) {
        this->Edges.clear();
        this->Subgraphs.clear();
        this->ne = ne;
        this->nv = 0;
    }

    int countVertices() {
        int highest = Edges[0].A;

        for (size_t i = 0; i < Edges.size(); i++) {
            if (Edges[i].A > highest) highest = Edges[i].A;
            if (Edges[i].B > highest) highest = Edges[i].B;
        }

        return (highest - 65 + 1); // 65 - A w ASCII
    }

    void addEdge(bool who, bool oneWay, int A, int B, int fatigue) {
        if (oneWay) {
            Edges.push_back(Edge(who, A, B, fatigue));
        }
        else {
            Edges.push_back(Edge(who, A, B, fatigue));
            Edges.push_back(Edge(who, B, A, fatigue));
        }
        
    }

    void getMeetupPoints() {
        std::vector<int> meetupPoints;

        for (int i = 0; i < nv; i++) {
            //if ( (Subgraphs[0].stepsArray[nv - 1][i] > 0) && (Subgraphs[1].stepsArray[nv - 1][i] > 0)) std::cout << (char)(i + 65) << " ";
            if ((Subgraphs[0].stepsArray[nv - 1][i] >= 0) && (Subgraphs[1].stepsArray[nv - 1][i] >= 0)) meetupPoints.push_back(i);
        }

        

        if (meetupPoints.size() > 0) {
            std::vector<int> minMeetupPoints;
            minMeetupPoints.push_back(meetupPoints[0]);
            int minimum = Subgraphs[0].stepsArray[nv - 1][meetupPoints[0]] + Subgraphs[1].stepsArray[nv - 1][meetupPoints[0]];

            for (size_t i = 1; i < meetupPoints.size(); i++) {
                int sumUp = Subgraphs[0].stepsArray[nv - 1][meetupPoints[i]] + Subgraphs[1].stepsArray[nv - 1][meetupPoints[i]];
                
                if (sumUp == minimum) minMeetupPoints.push_back(meetupPoints[i]);
                if (sumUp < minimum) {
                    minimum = sumUp;
                    minMeetupPoints.clear();
                    minMeetupPoints.push_back(meetupPoints[i]);
                }
            }

            std::cout << minimum << " ";
            for (size_t i = 0; i < minMeetupPoints.size(); i++) {
                std::cout << (char)(minMeetupPoints[i] + 65) << " ";
            }
        }
        else {
            std::cout << "Do spotkania nie dojdzie";
        }
        std::cout << "\n";
    }

    void printEdges() {
        for (size_t i = 0; i < Edges.size(); i++) {
            std::cout << "Edge " << Edges[i].who << ": " << (char)Edges[i].A << " -> " << (char)Edges[i].B << ": " << Edges[i].fatigue << "\n";
        }
    }

    void solve(int s0, int s1) { // s0 - start student, s1 - start nauczyciel
        for (int i = 0; i < 2; i++) {
            Subgraphs.push_back(Subgraph());
        }

        for (size_t i = 0; i < Edges.size(); i++) {
            if (Edges[i].who) {
                Subgraphs[1].addEdge(Edges[i]);
            }
            else {
                Subgraphs[0].addEdge(Edges[i]);
            }
        }

        this->nv = countVertices();

        //std::cout << "GRAF STUDENT\n";
        Subgraphs[0].init(nv);
        Subgraphs[0].createFatigueArray();
        //Subgraphs[0].showFatigueArray();
        //std::cout << "\n";
        Subgraphs[0].createStepsArray(s0);
        //Subgraphs[0].showStepsArray();
        //std::cout << "\n\n";

        //std::cout << "GRAF NAUCZYCIEL\n";
        Subgraphs[1].init(nv);
        Subgraphs[1].createFatigueArray();
        //Subgraphs[1].showFatigueArray();
        //std::cout << "\n";
        Subgraphs[1].createStepsArray(s1);
        //Subgraphs[1].showStepsArray();
        //std::cout << "\n\n";

        //std::cout << "MEETUP POINTS\n";
        //std::cout << "STUDENT:\n";
        //Subgraphs[0].getMeetupPoints();
        //std::cout << "NAUCZYCIEL:\n";
        //Subgraphs[1].getMeetupPoints();
        //std::cout << "RAZEM:\n";
        getMeetupPoints();


    }
};

int main()
{
    Graph G;

    int ne = 0, fatigue = 0;
    bool who = 0; // 0 - student / 1 - nauczyciel
    bool oneWay = 0; // 0 - dwukierunkowa / 1 - jednokierunkowa
    char whoChar, oneWayChar, A, B;

    while(true) {
        std::cin >> ne;
        if (ne <= 0) break;

        G.init(ne);

        for (int i = 0; i < ne; i++) {
            std::cin >> whoChar;
            who = (whoChar == 'M') ? 0 : 1;
                
            std::cin >> oneWayChar;
            oneWay = (oneWayChar == 'D') ? 0 : 1;
                
            std::cin >> A >> B >> fatigue;

            G.addEdge(who, oneWay, A, B, fatigue);
        }

        std::cin >> A >> B;
        G.solve(A, B);
    }

    return 0;
}